#include <stdio.h>
#include <stdlib.h>

int find_leap_year(int day, int month, int year);
int judge_ly (int year);

void aeon_players_day(int day, int month, int year){
    int m[]={31,28,31,30,31,30,31,31,30,31,30,31};
    //printf("%d/%d/%d\n",day,month,year);
    if (judge_ly(year) && year>1752){
            m[1]=29;}//判断是否是闰年，调整2月日期
    if ( day > 2 && day < 14 && month == 9 && year == 1752){//法案内日期invalid
        printf("Invalid date\n");
        return 0;
        }
    //printf("%d/%d/%d\n",day,month,year);
if (year>0 && month>0 && month<13 && day<=m[month-1] && day>0){//找到valid date
    if (year<=1756 && month<3 && day<29){
            printf("%02d/%02d/%d becomes %02d/%02d/%d\n",day,month,year,day,month,year);return 0;
    }//1756年之前无变化
    m[1]=28;
    int exdays = 0;
    int oy=year, om=month,od=day;
    if (day==29 && month==2){//要命的2.29
        day = 28;
        exdays += 1;
    }
    exdays += find_leap_year(day,month,year);

    //printf("%d/%d/%d/%d\n",day,month,year,exdays);
    while (exdays>=365){//减去年份
        year += 1;
        exdays -= 365;
      //  printf("%d/%d/%d/%d\n",day,month,year,exdays);
    }
    exdays += day-1;
    day = 1;
    //    printf("%d/%d/%d/%d\n",day,month,year,exdays);
    for (int i = month-1;exdays >= m[i]; i++){//计算月份
        exdays -= m[i];
        month += 1;
        //printf("%d/%d/%d/%d\n",day,month,year,exdays);
        if (month == 13){//防止月份爆棚
            month = 1;
            year += 1;
        //printf("%d/%d/%d/%d\n",day,month,year,exdays);
        }

    }
    day += exdays;
    //printf("%d/%d/%d/%d\n",day,month,year,exdays);
    if (day > m [month-1]){//计算日期，并防止爆棚
        day -= m [month-1];
        month += 1;
    }
    if (month == 13){
    month = 1;
    year += 1;
    }
    printf("%02d/%02d/%d becomes %02d/%02d/%d\n",od,om,oy,day,month,year);
    return 0;
    }

    else {printf("Invalid date\n");return 0;}
}

int judge_ly (int year){
    int flag = (year%4==0 && year%100!=0) || (year%400==0);//leap year returns
    return flag;

}
int find_leap_year(int day, int month, int year){
    int diff = year;
    if (judge_ly(year)==1 && month<3){diff -= 1;}
    int ly = diff/4-diff/100+diff/400-425;
    return ly;
}